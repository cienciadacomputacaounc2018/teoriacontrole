﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrapLauncher : MonoBehaviour {

	// Use this for initialization
	float rotation;

	void OnCollisionEnter(Collision coll){
		if (coll.gameObject.tag!="Enemy")
			return;
	
		coll.gameObject.GetComponent<Rigidbody> ().AddForce(new Vector3(0,500,0),ForceMode.Impulse);

		Debug.Log ("entrou");
		rotation = 3;

		StartCoroutine (Launch ());
	}

	IEnumerator Launch(){
		while (rotation>0) {
			transform.Rotate (new Vector3 (5, 0, 0));
			rotation -= Time.deltaTime;
			yield return null;
		}
	}


}
