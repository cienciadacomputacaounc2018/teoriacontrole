﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {


	public bool hit;
	Rigidbody rb;
	public Transform target;
	public float speed;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}

	void Update(){
		transform.LookAt (target);
		rb.MovePosition (Vector3.MoveTowards (transform.position, target.position, speed*Time.deltaTime));

		//transform.Translate(Vector3.forward*(speed * Time.deltaTime));
		
	}
	

}
